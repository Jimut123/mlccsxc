# MLCCSXC 

We will aim at completing this course [MLCC](https://developers.google.com/machine-learning/crash-course/) from google. Check the prerequisites before taking this code jam. This will be conducted in SXC, Kolkata, under science association. We will use Google's [Tensorflow](https://github.com/tensorflow/tensorflow) API as the basic API. We will use Google's [colab](https://colab.research.google.com/) for coding purposes. Participants are requested to share and code on colab along with some basic knowledge of [python](https://github.com/Jimut123/PYTHON_RESOURCES) programming language. 

## Syllabus

* [intended syllabus and study plan](https://gitlab.com/Jimut123/mlccsxc/tree/master/SYLLABUS)

## Prerequisites

* [complusory](https://developers.google.com/machine-learning/crash-course/prereqs-and-prework)

## Intended Instructors

* [Jimut Bahan Pal](https://www.linkedin.com/in/jimut-bahan-pal-156862123/)
* [Aniket Bhattacharyea](https://www.linkedin.com/in/abhattacharyea/)
* [Avijit Chakraborty](https://www.linkedin.com/in/avijit-chakraborty-bab78514a/)


